# OS detection
function is_osx() {
  [[ "$OSTYPE" =~ ^darwin ]] || return 1
}
function is_debian() {
  # Abort if not debian based
  OS="$(cat /etc/*release 2> /dev/null)"

  [[ "${OS}" =~ Ubuntu ]] || \
  [[ "${OS}" =~ Mint   ]] || \
  [[ "${OS}" =~ Debian ]] || return 1
}
function is_redhat() {
  # Abort if not debian based
  OS="$(cat /etc/*release 2> /dev/null)"

  [[ "${OS}" =~ CentOS    ]] || \
  [[ "${OS}" =~ "Red Hat" ]] || return 1
}
function get_os() {
  for os in osx debian redhat; do
    is_$os; [[ $? == ${1:-0} ]] && echo $os
  done
}
